"use client";
import Header from "./components/Header";
import { Roboto } from "next/font/google";
import Introduction from "./components/Introduction";
import AboutMe from "./components/AboutMe";
import Skills from "./components/Skills";
import Project from "./components/Project";
import Companies from "./components/Companies";
import useCheckMobileScreen from "./hooks/useCheckMobile";
import HeaderMobile from "./components/HeaderMobile";
import CompanyMobile from "./components/CompanyMobile";
import Modal from "./components/Modal";
import useModal from "./hooks/useModal";

const roboto = Roboto({
  weight: ["400", "400", "500", "700"],
  subsets: ["latin"],
});

export default function Home() {
  const isMobile = useCheckMobileScreen();
  const productModal = useModal();
  const layoutDesktop = () => {
    return (
      <>
        <div className="container">
          <Header />
          <Introduction />
          <AboutMe />
        </div>
        <Skills />
        <Project />
        <Companies />
      </>
    );
  };
  const layoutMobile = () => {
    return (
      <>
        <div className="mobile-mode">
          <HeaderMobile />
          <Introduction />
          <AboutMe />
        </div>
        <Skills />
        <Project />
        <CompanyMobile />
      </>
    );
  };
  return (
    <div className={roboto.className}>
      {isMobile ? layoutMobile() : layoutDesktop()}
      <Modal isOpen={productModal.isOpen} onClose={productModal.onClose} />
    </div>
  );
}
