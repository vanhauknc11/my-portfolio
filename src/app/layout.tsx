import "./globals.css";

export const metadata = {
  title: "Vanhauknc Portfolio | Full Stack Developer",
  description:
    "I'm Hau Nguyen and You can call me by vanhauknc. I'm a Full Stack Developer & there are my portfolio.",
  openGraph: {
    title: "Vanhauknc Portfolio | Full Stack Developer",
    description:
      "I'm Hau Nguyen and You can call me by vanhauknc. I'm a Full Stack Developer & there are my portfolio.",
    url: "https://my-portfolio-sable-nine.vercel.app/",
    siteName: "vanhauknc",
    images: [
      {
        url: "/images/logo/vanhauknc.png",
        width: 800,
        height: 600,
      },
      {
        url: "/images/logo/vanhauknc.png",
        width: 1800,
        height: 1600,
        alt: "My custom alt",
      },
    ],
    locale: "en-US",
    type: "website",
  },
  content: "width=device-width, initial-scale=1",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
