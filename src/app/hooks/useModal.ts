import { create } from "zustand";
import { ProjectType } from "../components/Project";

interface ModalStore {
  isOpen: boolean;
  data: ProjectType | undefined;
  onOpen: (data: ProjectType) => void;
  onClose: () => void;
}

const useModal = create<ModalStore>((set) => ({
  isOpen: false,
  data: undefined,
  onOpen: (data: ProjectType) => set({ isOpen: true, data }),
  onClose: () => set({ isOpen: false }),
}));

export default useModal;
