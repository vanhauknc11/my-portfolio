import React from 'react';
import useModal from '../hooks/useModal';

interface ModalProps {
    isOpen?: boolean;
    onClose?: () => void;

}

const Modal: React.FC<ModalProps> = ({ isOpen, onClose }) => {
    const [showModal, setShowModal] = React.useState(isOpen);
    const { data } = useModal();

    React.useEffect(() => {
        setShowModal(isOpen);
    }, [isOpen]);

    const handleClose = () => {
        setShowModal(false);
        onClose && onClose();
    }

    if (!isOpen) {
        return null;
    }
    return (
        <>
            <div className='justify-center items-center flex
             overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-neutral-800/70'>
                <div className='relative w-full md:w-4/6 my-6 mx-auto h-full lg:h-auto md:h-auto'>
                    <div className={`translate duration-300 h-full  
                    ${showModal ? "translate-y-0" : "translate-y-full"}
                    ${showModal ? "opacity-100" : "opacity-0"}`}>
                        <div className='translate h-auto lg:h-auto md:h-auto border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none'>
                            <div className='relative min-h-[600px] rounded-lg'>
                                <div className='absolute text-gray-400 right-0 top-0 md:right-[-10px] md:top-[-20px] bg-white px-4 py-2 border-zinc-500 border-[3px] rounded-full cursor-pointer z-30' onClick={handleClose}><span className='hover:opacity-50 transition'>X</span></div>
                                <div className='modal-content rounded-lg'>
                                    <div className='rounded-lg bg-cover bg-no-repeat flex items-center justify-center' style={{ backgroundImage: `url('${data?.image}')`, height: "600px" }}>
                                        <div className='flex flex-col justify-center p-6 relative w-full h-full product-detail-card'>
                                            <div className='flex flex-col justify-center gap-5 max-w-lg z-10'>
                                                <h2 className='text-3xl font-medium'>{data?.name}</h2>
                                                <sub className='text-gray-400'>{data?.type}</sub>
                                                <p className='text-sm'>{data?.description}</p>
                                                <ul className='product-features'>
                                                    {
                                                        data?.features?.map((feature, index) => {
                                                            return <li key={index}>{feature}</li>
                                                        })
                                                    }
                                                </ul>
                                                {data?.url && (
                                                    <div className='px-4 py-3 border-white border-[1px] max-w-[160px] text-center rounded-lg cursor-pointer '>
                                                        <a href={data?.url} target="_blank" className='hover:opacity-70 transition '>Take a look</a>
                                                    </div>
                                                )}

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </>
    );
};

export default Modal;