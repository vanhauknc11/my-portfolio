import React from "react";
import { Courier_Prime } from "next/font/google";
import useCheckMobileScreen from "../hooks/useCheckMobile";
import useModal from "../hooks/useModal";
const courierPrime = Courier_Prime({
  weight: ["400", "700"],
  subsets: ["latin"],
});
export type ProjectType = {
  name: string;
  description: string;
  image: string;
  type: "Personal Project" | `Company Project`;
  url: string;
  features?: string[];
};
const projects: ProjectType[] = [
  {
    name: "InfiniX Project",
    description:
      "InfiniX is a project that provides a platform for users using Zalo ZNS services include create ZNS campaign, manage templates, manage reports, and manage the account.",
    image: "/images/InfiniX.png",
    type: "Company Project",
    url: "https://portal.infinix.vn/",
    features: [
      "Handle large traffics",
      "Manage multi-tenants, multi-roles",
      "Easy to integrate with APIs",
    ],
  },
  {
    name: "eWebinar",
    description:
      "eWebinaran an automated webinar platform that combines pre-recorded video with real-time interactions and live chat to deliver a delightfully engaging experience for attendees.",
    image: "/images/webinar.png",
    type: "Company Project",
    url: "https://ewebinar.com/",
    features: [
      "Highly customize interaction of videos",
      "Easy to sharing video to attendee",
      "Multi-management and analysis of feedback results",
    ],
  },
  {
    name: "E-Tradding",
    description:
      "This is a electronic trading platform allowing customer buy and sell gold or foreign currency online owned by the bank",
    image: "/images/mbbank.png",
    type: "Company Project",
    url: "https://etrading.eximbank.com.vn/",
    features: [
      "Strictly security",
      "Realtime show exchange & sell,buy",
      "Application can be work well under huge traffic in time",
    ],
  },
  {
    name: "AI SasS Tool using OpenAI services",
    description:
      "This is an AI SasS web tool, providing diverse services such as creating chats, asking questions, generating images, videos or audio and especially is always free.",
    image: "/images/sass-ai.png",
    type: "Personal Project",
    url: "",
    features: [
      "Easy creating asking questions ",
      "Easy creating conversations",
      "Easy generate images, video, audio",
      "No need credit card",
      "Everything is supper fast",
    ],
  },
  {
    name: "Captcha Solved using Machine Learning technical",
    description:
      "This is a web tool that provides auto captcha solution with many types of support, Machine learning application for training and object detection.",
    image: "/images/captcha-resolve.png",
    type: "Personal Project",
    url: "https://captcha-server.vercel.app/",
    features: [
      "Provide Api to easy integrate with any flatform",
      "Support multi types captcha",
      "Everything is free",
      "Everything is supper fast",
    ],
  },

  {
    name: "Airbnb Clone : Booking HomeStay and Hotel For Vacation Website",
    description:
      "Website was clone from airbnb.com with the same futures like register account, login, booking room and room rental, manage reservation, favorite place...etc",
    image: "/images/air-bnb.png",
    type: "Personal Project",
    url: "",
    features: [
      "Optimize with Sever x Client side rendering",
      "Responsive with diversey screens",
      "Easily manage personal resources",
    ],
  },
];

const Project = () => {
  const isMobile = useCheckMobileScreen();
  const productModal = useModal();
  return (
    <div className="projects mobile-mode" id="projects">
      <div className={!isMobile ? "container" : ""}>
        <h2 className="title">
          projects<span>( )</span>
        </h2>
        <div className="card-list">
          {projects.map((project, index) => (
            <div
              className="card"
              key={index}
              onClick={() => productModal.onOpen(project)}
            >
              <img src={project.image} alt={project.name} />
              <div className={`${courierPrime.className} card-content`}>
                <div className="card-content-desc">
                  <p style={{ color: "gray" }}>{project.type}</p>
                  <h2>{project.name}</h2>
                  <p>{project.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Project;
