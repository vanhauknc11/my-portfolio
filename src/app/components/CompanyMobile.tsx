import React from "react";

const CompanyMobile = () => {
  return (
    <div className="companies spacing-company mobile-mode">
      <div>
        <h2 className="title">
          companies<span>( )</span>
        </h2>
        <div className="mobile-company-item">
          <h3>Fullstack Developer at VNG Corporation</h3>
          <p>01/2024 - Present</p>
          <p>
            Here I take on the role of fullstack development, both frontend and
            backend and build infrastructure for products like Zalo Services,
            Chatbot... . I learned a lot of cool things while working here such
            as system design, handle large traffics and microservices.
          </p>
        </div>
        <div className="mobile-company-item">
          <h3>
            Fullstack Developer at HDWebsoft - Software Development Company
          </h3>
          <p>04/2022 - 01/2024</p>
          <p>
            I was given full time to work on the Front-End side, so my skills
            leveled up a new level. Get access to great challenges and solutions
            from mentors and professional co-workers. Get updated skill sets as
            well as the latest technologies around the world. The projects are
            diverse, so my flexibility is also improved
          </p>
        </div>
        <div className="mobile-company-item">
          <h3>Full Stack Developer at Financial Software Solutions (FSS)</h3>
          <p>06/2021 - 03/2022</p>
          <p>
            Here I take on the role of website development, both frontend and
            backend and build infrastructure for products like stock exchange,
            bank products. I learned a lot of cool things while working here
            such as banking security issues, and further improve skills in using
            Javascript as well as SQL queries
          </p>
        </div>
      </div>
    </div>
  );
};

export default CompanyMobile;
