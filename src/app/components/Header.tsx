"use client";
import Image from "next/image";
import React from "react";

const Header = () => {
  const handleScroll = (id: string) => {
    const element = document.querySelector(`#${id}`);
    element?.scrollIntoView({ behavior: "smooth", block: "start" });
  };
  return (
    <div className="header">
      <div className="logo">
        VANHAU<span>KNC</span>
      </div>
      <div style={{ display: "flex" }}>
        <ul className="text-menu">
          <li
            onClick={() => {
              handleScroll("about");
            }}
          >
            About
          </li>
          <li
            onClick={() => {
              handleScroll("skills");
            }}
          >
            Skills
          </li>
          <li
            onClick={() => {
              handleScroll("projects");
            }}
          >
            Projects
          </li>
        </ul>
        <ul className="contact-menu">
          <li>
            <a
              href="https://www.linkedin.com/in/vanhauknc/"
              target="_blank"
            >
              <Image
                src={"/images/linkedin.png"}
                width={25}
                height={25}
                alt="Linkedin"
              />
              Linkedin
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/vanhauknc11" target="_blank">
              <Image
                src={"/images/github.png"}
                width={25}
                height={25}
                alt="Github"
              />
              Github
            </a>
          </li>
          <li>
            <a href="/HAU-NGUYEN-FULLSTACK.pdf" download={true}>
              <Image
                src={"/images/email-fast-outline.png"}
                width={25}
                height={25}
                alt="Email"
              />
              Download CV
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Header;
