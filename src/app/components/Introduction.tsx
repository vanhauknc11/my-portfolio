import Image from "next/legacy/image";
import React from "react";
import useCheckMobileScreen from "../hooks/useCheckMobile";

const Introduction = () => {
  return (
    <div className="introduction">
      <div className="intro-text">
        <h3>{"Hello, i am"} </h3>
        <h2>{`< Hau `}</h2>
        <h2>{`Nguyen />`}</h2>
        <h3>Fullstack Developer</h3>
        <div className="intro-details">
          <div className="intro-detail">
            <span className="detail-number">4+</span>{" "}
            <span className="detail-text width-100">year of experience</span>
          </div>
          <div className="intro-detail">
            <span className="detail-number">8</span>{" "}
            <span className="detail-text">
              Project completed around the world
            </span>
          </div>
        </div>
      </div>
      <div className="intro-image">
        <div
          className="bg-avatar"
          style={{
            width: "100%",
            height: "100%",
            position: "relative",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="avatar-div">
            <Image
              className="avatar-transition"
              style={{ borderRadius: "25px" }}
              src={"/images/avatar-image.JPG"}
              alt="Avatar"
              width={400}
              height={450}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Introduction;
