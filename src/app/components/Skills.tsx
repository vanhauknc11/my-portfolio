import Image from "next/image";
import React from "react";
import useCheckMobileScreen from "../hooks/useCheckMobile";

type logoType = {
  imageName: string;
  toolTip: string;
};
const Skills = () => {
  const logoList: logoType[] = [
    { imageName: "javascript-original.png", toolTip: "JavaScript" },
    { imageName: "typescript-original.png", toolTip: "TypeScript" },
    { imageName: "html5-original.png", toolTip: "Html" },
    { imageName: "css3-original.png", toolTip: "Css" },
    { imageName: "nextjs-original.png", toolTip: "NestJs" },
    { imageName: "nestjs-logo.png", toolTip: "Nextjs" },
    { imageName: "react-original.png", toolTip: "ReactJs" },
    { imageName: "Tailwind_CSS_Logo.svg.png", toolTip: "Tailwind" },
    { imageName: "redux-original.png", toolTip: "Redux" },
    { imageName: "GraphQL_Logo.svg.png", toolTip: "GraphQL" },
    { imageName: "linux-original.png", toolTip: "Linux" },
    { imageName: "Moby-logo.webp", toolTip: "Docker" },
    { imageName: "mysql-logo.png", toolTip: "Mysql" },
    { imageName: "langchain.jpg", toolTip: "Langchain" },
    { imageName: "aws.png", toolTip: "Amazon Service" },
  ];
  const isMobile = useCheckMobileScreen();
  return (
    <div className="skills mobile-mode" id="skills">
      <div className={!isMobile ? `container` : ""}>
        <h2 className="title">
          skills<span>( )</span>
        </h2>

        <div className="skill-logo">
          {logoList.map((logo, index) => (
            <div className="skill-item" key={index}>
              <Image
                src={`/images/logo/${logo.imageName}`}
                alt={logo.toolTip}
                width={75}
                height={75}
              />
              <span className="tool-tip">{logo.toolTip}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Skills;
