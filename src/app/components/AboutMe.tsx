"use client";
import React from "react";
import { Courier_Prime } from "next/font/google";
import { handleScroll } from "../helpers";
const courierPrime = Courier_Prime({
  weight: ["400", "700"],
  subsets: ["latin"],
});
const AboutMe = () => {
  return (
    <div className="about-me" id="about">
      <div>
        <h2 className="title">
          aboutMe<span>( )</span>
        </h2>

        <p>
          {`Hello, I'm Hau Nguyen—an experienced web developer of over 4 years. 
          Proficient in both front-end and back-end Javascript development, I also dabble in DevOps and System architecture. 
          To me, programming is more than code; It includes creativity and problem-solving skills.`}
        </p>
      </div>
      <div className={courierPrime.className}>
        <div className="about-me-detail">
          <div className="me-job">
            <h4>Fullstack Developer</h4>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                handleScroll("about");
              }}
            >
              Projects
            </a>
          </div>
          <div className="symbol">{`</>`}</div>
        </div>
        <div className="about-me-detail">
          <div className="me-job">
            <h4>Freelancer</h4>
            <a href="https://www.linkedin.com/in/vanhauknc/" target="_blank">
              Hire me!
            </a>
          </div>
          <div className="symbol">{`{}`}</div>
        </div>
      </div>
    </div>
  );
};

export default AboutMe;
